/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author waran
 */
public class OXunittest {
    
    public OXunittest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
     @Test
    public void checkWin_O_win_horizontal_1() {
        char Board[][] = {{'O', 'O', 'O'}, {'4', '5', '6'}, {'7', '8', '9'}};
        String result = Lab3.checkWin(Board);
        assertEquals("O Win", result);
    }
    @Test
     public void checkWin_O_win_horizontal_2() {
        char Board[][] = {{'4', '5', '6'}, {'O', 'O', 'O'}, {'7', '8', '9'}};
        String result = Lab3.checkWin(Board);
        assertEquals("O Win", result);
    }
      @Test
    public void checkWin_O_win_horizontal_3() {
        char Board[][] = {{'7', '8', '9'}, {'4', '5', '6'}, {'O', 'O', 'O'}};
        String result = Lab3.checkWin(Board);
        assertEquals("O Win", result);
    }
        @Test
    public void checkWin_O_win_vertical_1() {
        char Board[][] = {{'O', '5', '6'}, {'O', '8', '9'}, {'O', '2', '3'}};
        String result = Lab3.checkWin(Board);
        assertEquals("O Win", result);
    }
      @Test
    public void checkWin_O_win_vertical_2() {
        char Board[][] = {{'1', 'O', 'X'}, {'4', 'O', '6'}, {'7', 'O', 'X'}};
        String result = Lab3.checkWin(Board);
        assertEquals("O Win", result);
    }
    @Test
    public void checkWin_O_win_vertical_3() {
        char Board[][] = {{'X', '5', 'O'}, {'X', '8', 'O'}, {'7', '2', 'O'}};
        String result = Lab3.checkWin(Board);
        assertEquals("O Win", result);
    }
    @Test
    public void checkWin_O_win_diagonal_1() {
        char Board[][] = {{'O', '5', 'X'}, {'4', 'O', '6'}, {'X', '8', 'O'}};
        String result = Lab3.checkWin(Board);
        assertEquals("O Win", result);
    }
     @Test
    public void checkWin_O_win_diagonal_2() {
        char Board[][] = {{'X', '5', 'O'}, {'4', 'O', '6'}, {'O', '8', 'X'}};
        String result = Lab3.checkWin(Board);
        assertEquals("O Win", result);
    }
     @Test
    public void checkWin_X_win_horizontal_1() {
        char Board[][] = {{'X', 'X', 'X'}, {'4', '5', '6'}, {'7', '8', '9'}};
        String result = Lab3.checkWin(Board);
        assertEquals("X Win", result);
    }
    @Test
    public void checkWin_X_win_horizontal_2() {
        char Board[][] = {{'4', '5', '6'}, {'X', 'X', 'X'}, {'7', '8', '9'}};
        String result = Lab3.checkWin(Board);
        assertEquals("X Win", result);
    }
    @Test
    public void checkWin_X_win_horizontal_3() {
        char Board[][] = {{'7', '8', '9'}, {'4', '5', '6'}, {'X', 'X', 'X'}};
        String result = Lab3.checkWin(Board);
        assertEquals("X Win", result);
    }
    @Test
    public void checkWin_X_win_vertical_1() {
        char Board[][] = {{'X', '5', '6'}, {'X', '8', '9'}, {'X', '2', '3'}};
        String result = Lab3.checkWin(Board);
        assertEquals("X Win", result);
    }
    @Test
    public void checkWin_X_win_vertical_2() {
        char Board[][] = {{'1', 'X', 'X'}, {'4', 'X', '6'}, {'7', 'X', 'X'}};
        String result = Lab3.checkWin(Board);
        assertEquals("X Win", result);
    }
 @Test
    public void checkWin_X_win_vertical_3() {
        char Board[][] = {{'X', '5', 'X'}, {'X', '8', 'X'}, {'7', '2', 'X'}};
        String result = Lab3.checkWin(Board);
        assertEquals("X Win", result);
    }
    @Test
    public void checkWin_X_win_diagonal_1() {
        char Board[][] = {{'X', '5', 'X'}, {'4', 'X', '6'}, {'X', '8', 'X'}};
        String result = Lab3.checkWin(Board);
        assertEquals("X Win", result);
    }
    @Test
    public void checkWin_X_win_diagonal_2() {
        char Board[][] = {{'4', '5', 'X'}, {'X', 'X', '6'}, {'X', '8', '9'}};
        String result = Lab3.checkWin(Board);
        assertEquals("X Win", result);
    }
@Test
    public void checkTie_true() {
        char[][] gameBoard = {{'X', 'O', 'X'}, {'O', 'O', 'X'}, {'X', 'X', 'O'}};
        boolean result = Lab3.checktie(gameBoard);
        assertEquals(true,result);
    }
@Test
    public void checkTie_false() {
        char[][] gameBoard = {{'1', 'O', 'X'}, {'O', 'O', 'X'}, {'X', 'X', 'O'}};
        boolean result = Lab3.checktie(gameBoard);
        assertEquals(false,result);
    }
}
